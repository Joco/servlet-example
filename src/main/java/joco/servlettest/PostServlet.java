package joco.servlettest;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

public class PostServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();
        out.print(convertToJsonString(createResponseBody()));
        out.flush();
    }

    private PostResponseBody createResponseBody() {
        PostResponseBody response = new PostResponseBody();
        response.setMessage("hello post");
        return response;
    }

    private String convertToJsonString(PostResponseBody response) throws IOException {
        ObjectMapper om = new ObjectMapper();
        return om.writeValueAsString(response);
    }

    private static class PostResponseBody {
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }
}
