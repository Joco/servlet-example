package joco.servlettest;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

public class GetServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();
        out.print(convertToJsonString(createResponseBody()));
        out.flush();
    }

    private GetResponseBody createResponseBody() {
        GetResponseBody response = new GetResponseBody();
        response.setMessage("hello get");
        return response;
    }

    private String convertToJsonString(GetResponseBody response) throws IOException {
        ObjectMapper om = new ObjectMapper();
        return om.writeValueAsString(response);
    }

    private static class GetResponseBody {
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }
}
